from . import db


class Pago(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(64), nullable=False)
    code = db.Column(db.String(64))
    active = db.Column(db.Boolean)
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
    user = db.relationship('User', backref='pago_id')
    state = db.Column(db.String(10))
    monto = db.Column(db.Float)
    fees = db.Column(db.Float)
    date = db.Column(db.DateTime)

    medio_pago_id = db.Column(db.Integer, db.ForeignKey('medio_pago.id'))
    medio_pago = db.relationship('MedioPago')

    def __str__(self):
        return "[{}] {}".format(self.code, self.name)
