from app.models import db
from app.models.moneda import Moneda
from .admin import admin
from .my_model_view import MyModelView
import flask_login as login


class MonedaAdmin(MyModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated and login.current_user.has_role('admin')


admin.add_view(MonedaAdmin(Moneda, db.session, category="Other"))
