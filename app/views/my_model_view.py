from flask_admin.contrib import sqla
import flask_login as login


# Create customized model view class
class MyModelView(sqla.ModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated
