from app.models import db
from app.models.acteco import Acteco
from .admin import admin
from .my_model_view import MyModelView
import flask_login as login


class ActecoAdmin(MyModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated and login.current_user.has_role('admin')

    column_default_sort = ('code', True)
    column_searchable_list = [
        'name',
        'code',
    ]


admin.add_view(ActecoAdmin(Acteco, db.session, category="Other"))
